package com.mypopsy.floatingsearchview.demo.dagger;

import com.mypopsy.floatingsearchview.demo.search.SearchController;
import javax.inject.Singleton;
import com.mypopsy.floatingsearchview.demo.slice.MainAbilitySlice;
import dagger.Component;

@Singleton
@Component(modules = {RetrofitModule.class, SearchModule.class})
public interface AppComponent {
    void inject(MainAbilitySlice mainActivity);
    SearchController getSearch();
}
