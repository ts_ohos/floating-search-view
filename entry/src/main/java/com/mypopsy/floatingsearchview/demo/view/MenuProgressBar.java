package com.mypopsy.floatingsearchview.demo.view;

import com.mypopsy.floatingsearchview.demo.Utils.ViewUtils;
import ohos.agp.components.Component;
import ohos.agp.components.ProgressBar;
import ohos.app.Context;

public class MenuProgressBar extends ProgressBar implements Component.EstimateSizeListener {

    public MenuProgressBar(Context context) {
        super(context);
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        setEstimatedSize(ViewUtils.vp2px(getContext(), 48), ViewUtils.vp2px(getContext(), 24));
        return true;
    }
}
