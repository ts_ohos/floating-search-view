package com.mypopsy.floatingsearchview.demo.Utils;


import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * Created by renaud on 01/01/16.
 */
public class ViewUtils {

    /**
     * 把密度转换为像素
     *
     * @param vpValue dp值
     * @return int
     */
    public static int vp2px(Context context, float vpValue) {

        float scale = getScreenDensity(context);
        return (int) (vpValue * scale + 0.5);
    }

    /**
     * 将像素转换成vp
     *
     * @param pxValue px值
     * @return int
     */
    public static int px2vp(Context context, float pxValue) {

        float scale = getScreenDensity(context);
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * 将fp值转换为px值，保证文字大小不变
     *
     * @param fpValue sp值
     * @return int
     */
    public static int fp2px(Context context, float fpValue) {

        float fontScale = getScreenDensity(context);
        return (int) (fpValue * fontScale + 0.5f);
    }

    /**
     * 得到设备的密度
     *
     * @param context ctx
     * @return float
     */
    public static float getScreenDensity(Context context) {
        DisplayAttributes attributes = DisplayManager
                .getInstance()
                .getDefaultDisplay(context)
                .get()
                .getRealAttributes();
        return attributes.densityPixels;
    }
}
