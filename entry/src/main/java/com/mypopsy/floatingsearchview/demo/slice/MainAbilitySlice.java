package com.mypopsy.floatingsearchview.demo.slice;

import com.mypopsy.floatingsearchview.demo.ResourceTable;
import com.mypopsy.floatingsearchview.demo.Utils.PackageUtils;
import com.mypopsy.floatingsearchview.demo.widget.RoundImage;
import com.mypopsy.widget.FloatingSearchView;
import com.mypopsy.widget.custom.TypePopupWindow;
import com.mypopsy.widget.pojo.MenuItem;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.media.image.PixelMap;
import ohos.utils.net.Uri;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {

    public static final String PROJECT_URL = "https://github.com/renaudcerrato/FloatingSearchView";
    private static final int REQ_CODE_SPEECH_INPUT = 42;

    private PositionLayout myLayout = new PositionLayout(this);

    private ComponentContainer rootContainer;
    private FloatingSearchView mSearchView;
    private RoundImage actionButton;

    private PixelMapElement searchElement;
    private PixelMapElement drawerElement;
    private PixelMapElement customElement;

    private static final int SUGGESTION_COUNT = 5;
    protected List<MenuItem> menus = new ArrayList<>();
    protected List<MenuItem> types = new ArrayList<>();

    private List<String> sColorSuggestions = new ArrayList<>(
            Arrays.asList("green", "blue", "pink", "purple", "brown", "gray",
                    "Granny Smith Apple", "Indigo", "Periwinkle",
                    "Mahogany", "Maize", "Mahogany", "Outer Space", "Melon",
                    "Yellow", "Orange", "Red", "Orchid"));
    private boolean searchFlag = false;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        initData();
        ComponentContainer.LayoutConfig config = new ComponentContainer.LayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_PARENT);
        myLayout.setLayoutConfig(config);
        Component rootContainerComponent = LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_ability_main, null, false);

        if (rootContainerComponent instanceof ComponentContainer) {
            rootContainer = (ComponentContainer) rootContainerComponent;
        }
        actionButton = (RoundImage) rootContainer.findComponentById(ResourceTable.Id_floatingActionButton);

        actionButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                PackageUtils.start(MainAbilitySlice.this, Uri.parse(PROJECT_URL));
            }
        });

        mSearchView = (FloatingSearchView) rootContainer.findComponentById(ResourceTable.Id_search);
        mSearchView.showLogo(true);
        mSearchView.setOnIconClickListener(new FloatingSearchView.OnIconClickListener() {
            @Override
            public void onNavigationClick() {
                mSearchView.changeNavButtonView();
            }
        });
        mSearchView.setOnSearchInputClickListener(new FloatingSearchView.onSearchInputClickListener() {
            @Override
            public void onSearchInputClick() {
                mSearchView.changeSearchInputView();
            }
        });

        mSearchView.setOnMenuItemClickListener(new FloatingSearchView.OnMenuItemClickListener() {
            @Override
            public void onMenuItemClick(Context context, Component component) {
                new TypePopupWindow(getContext(), component)
                        .setData(menus)
                        .setListener(new TypePopupWindow.OnProjectTypeClickListener() {
                            @Override
                            public void onProjectTypeClick(MenuItem menuItem) {
                                onOptionsItemSelectedMenu(menuItem, component);
                            }
                        })
                        .show(component);
            }
        });

        mSearchView.setOnMenuMicrophoneClickListener(new FloatingSearchView.OnMenuMicrophoneClickListener() {
            @Override
            public void onMenuMicrophoneClick() {
                PackageUtils.startTextToSpeech(MainAbilitySlice.this,
                        getString(ResourceTable.String_speech_prompt), REQ_CODE_SPEECH_INPUT);
            }
        });

        mSearchView.setText(null);

        FloatingSearchView.init(SUGGESTION_COUNT, sColorSuggestions, new FloatingSearchView.OnSuggestionClickListener() {
            @Override
            public void onSuggestionClick(String alert) {

            }
        });

        super.setUIContent(rootContainer);
    }

    private void initData() {
        menus.add(new MenuItem(0, "Icon Visible", true, true));
        menus.add(new MenuItem(1, "Icon Type", false, false));
        types.add(new MenuItem(0, "Search", false, false));
        types.add(new MenuItem(1, "Drawer", false, false));
        types.add(new MenuItem(2, "Custom", false, false));

        try {
            final Resource searchResource =
                    getResourceManager().getResource(ResourceTable.Media_ic_search);
            final Resource drawerResource =
                    getResourceManager().getResource(ResourceTable.Media_ic_drawer);
            final Resource customResource =
                    getResourceManager().getResource(ResourceTable.Media_ic_custom);
            searchElement = new PixelMapElement(searchResource);
            drawerElement = new PixelMapElement(drawerResource);
            customElement = new PixelMapElement(customResource);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        }
    }

    public boolean onOptionsItemSelectedMenu(MenuItem item, Component component) {

        new ToastDialog(getContext())
                .setText(item.getName())
                .show();

        switch (item.getId()) {
            case 0:
                item.setChecked(!item.isChecked());
                item.setCheckboxChecked(!item.isChecked());
                mSearchView.showIcon(!item.isChecked());
                break;
            case 1:
                new TypePopupWindow(getContext(), component)
                        .setData(types)
                        .setListener(new TypePopupWindow.OnProjectTypeClickListener() {
                            @Override
                            public void onProjectTypeClick(MenuItem menuItem) {
                                onOptionsItemSelectedType(menuItem);
                            }
                        })
                        .show(component);
                break;
        }
        return true;
    }

    private boolean onOptionsItemSelectedType(MenuItem menuItem) {
        new ToastDialog(getContext())
                .setText(menuItem.getName())
                .show();

        switch (menuItem.getId()) {
            case 0:
            case 1:
            case 2:
                updateNavigationIcon(menuItem.getId());
                break;
        }
        return true;
    }

    private void updateNavigationIcon(int itemId) {
        Element drawable = null;
        switch(itemId) {
            case 0:
                drawable = searchElement;
                break;
            case 1:
                drawable = drawerElement;
                break;
            case 2:
                drawable = customElement;
                break;
        }
        mSearchView.setIcon(drawable);
    }

    @Override
    public void onActive() {
        super.onActive();
        Image image = new Image(getContext());
        image.setPixelMap(ResourceTable.Media_ic_action_github);
        PixelMap pixelMap = image.getPixelMap();
        actionButton.setPixelMapAndCircle(pixelMap);
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
