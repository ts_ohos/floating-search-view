package com.mypopsy.floatingsearchview.demo.Utils;


import com.mypopsy.floatingsearchview.demo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.utils.net.Uri;

/**
 * Created by renaud on 01/01/16.
 */
public class PackageUtils {

    static public void start(Context context, Uri uri) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withUri(uri)
                .withAction("android.intent.action.VIEW")
                .withFlags(Intent.FLAG_NOT_OHOS_COMPONENT)
                .build();
        intent.setUriAndType(uri, null);
        intent.setOperation(operation);
        context.startAbility(intent, 0);
    }

    static public void startTextToSpeech(AbilitySlice context, String prompt, int requestCode) {
//        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
//        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
//                RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
//        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
//        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, prompt);
//        try {
//            context.startActivityForResult(intent, requestCode);
//        } catch (ActivityNotFoundException a) {
//            Toast.makeText(context, context.getString(R.string.speech_not_supported),
//                    Toast.LENGTH_SHORT).show();
//        }
        new ToastDialog(context)
                .setText(context.getString(ResourceTable.String_speech_not_supported))
                .setDuration(400)
                .show();
    }

}
