# FloatingSearchView
本项目是基于开源项目FloatingSearchView进行harmonyos化的移植和开发的，可以通过项目标签以及
[github地址](https://github.com/renaudcerrato/FloatingSearchView).


移植版本：源master版本

## 项目介绍
### 项目名称：FloatingSearchView
### 所属系列：harmonyos的第三方组件适配移植
### 功能：
    FloatingSearchView 是一个浮动搜索视图，该实现支持菜单（包括子菜单）、徽标和图标。


### 项目移植状态：完全移植
### 调用差异：由于鸿蒙不支持menu样式，部分属性直接在xml布局中赋值。
### 原项目Doc地址：https://github.com/renaudcerrato/FloatingSearchView
### 编程语言：java

### 项目demo截图
![鸿蒙运行效果](art/screenPicture.png)  
![鸿蒙运行效果](art/demo.gif)

### 安装教程
    
#### 方案一
建议下载开源代码并参照demo引入相关库：
```groovy
 dependencies {
    implementation project(':library')
 }  
```
    
#### 方案二
    
* 项目根目录的build.gradle中的repositories添加：
```groovy
 buildscript {
       repositories {
           ...
           mavenCentral()
       }
       ...
   }
   
   allprojects {
       repositories {
           ...
           mavenCentral()
       }
   }
```
    
* module目录的build.gralde中dependencies添加：
```groovy
 dependencies {
     implementation 'com.gitee.ts_ohos:floatingsearchviewlibrary:1.0.1' 
 }
``` 
    
## 使用说明
* 属性选项
```xml
    //搜索框默认文字
    app:fsv_searchHint="Search" 
    //搜索框logo
    app:logo="$media:logo"  
    //组件左侧图标
    app:fsv_icon="$media:ic_search"  
    //组件右侧图标
    app:fsv_microphone="$media:ic_microphone" 
    //搜索框清除文字图标 
    app:fsv_clear="$media:ic_clear" 
    //组件右侧菜单图标
    app:fsv_menu="$media:ic_menu"  
    //组件左侧返回图标
    app:fsv_back="$media:ic_back"  
```
* 功能具体实现请根据 demo （entry）部分实现构建
    
## 例子

* XML实现
```xml
    <com.mypopsy.widget.FloatingSearchView
        ohos:id="$+id:search"
        ohos:width="match_parent"
        ohos:height="match_content"
        app:fsv_searchBarMarginLeft="5"
        app:fsv_searchBarMarginTop="5"
        app:fsv_searchBarMarginRight="5"
        app:fsv_searchHint="Search"
        app:logo="$media:logo"
        app:fsv_icon="$media:ic_search"
        app:fsv_microphone="$media:ic_microphone"
        app:fsv_clear="$media:ic_clear"
        app:fsv_menu="$media:ic_menu"
        app:fsv_back="$media:ic_back"/>
```

License
--------

                                 Apache License
                           Version 2.0, January 2004
                        http://www.apache.org/licenses/


