/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mypopsy.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.TextField;
import ohos.agp.components.element.Element;
import ohos.agp.render.Canvas;
import ohos.app.Context;


public class LogoTextField extends TextField implements Component.DrawTask {

    private boolean logoShown;
    private Element logo;

    public LogoTextField(Context context) {
        this(context, null);
    }

    public LogoTextField(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public LogoTextField(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        addDrawTask(this::onDraw);
    }

    public void showLogo(boolean show) {
        logoShown = show;
    }

    public boolean isLogoShown() {
        return logoShown;
    }

    public void setLogo(Element logo) {
        this.logo = logo;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (logoShown && logo != null) {
            updateLogoBounds();
            logo.drawToCanvas(canvas);
        }
    }

    private void updateLogoBounds() {
//        int logoHeight = Math.min(getHeight(), logo.getHeight());
//        int top = (getHeight() - logoHeight)/2;
//        int logoWidth = (logo.getWidth() * logoHeight)/logo.getHeight();
        logo.setBounds(0, 50, 180, 120);
    }
}
