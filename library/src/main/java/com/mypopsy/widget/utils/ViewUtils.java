package com.mypopsy.widget.utils;

import java.lang.reflect.Method;

public class ViewUtils {

    public static void showKeyboard() {
        try {
            Class inputClass = Class.forName("ohos.miscservices.inputmethod.InputMethodController");
            Method method = inputClass.getMethod("getInstance");
            Object object = method.invoke(new Object[]{});
            Method startInput = inputClass.getMethod("startInput", int.class, boolean.class);
            startInput.invoke(object, 1, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void hideKeyboard() {
        try {
            Class inputClass = Class.forName("ohos.miscservices.inputmethod.InputMethodController");
            Method method = inputClass.getMethod("getInstance");
            Object object = method.invoke(new Object[]{});
            Method stopInput = inputClass.getMethod("stopInput", int.class);
            stopInput.invoke(object, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
