/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mypopsy.widget.suggestions;

import com.mypopsy.widget.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;
import java.util.List;

/**
 * SearchSuggestionsAdapter extends BaseItemProvider
 */
public class SearchSuggestionsAdapter extends BaseItemProvider {
    /**
     * onItemClicklistener onItemClicklistener
     */
    public OnItemClicklistener onItemClicklistener;

    private Context context;

    private List<String> itemList;

    /**
     * OnItemClicklistener interface for click listener
     */
    public interface OnItemClicklistener {
        void onItemClick(String position, boolean isRight);
    }

    public SearchSuggestionsAdapter(Context context, List<String> itemList,
                                    OnItemClicklistener onItemClicklistener) {
        this.context = context;
        this.itemList = itemList;
        this.onItemClicklistener = onItemClicklistener;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component,
                                  ComponentContainer componentContainer) {
        return getRootView(position);
    }

    private Component getRootView(int position) {
        Component rootView = LayoutScatter.getInstance(context)
            .parse(ResourceTable.Layout_search_suggestion_item, null, false);
        Component dlSuggestionItemComp = rootView.findComponentById(
                ResourceTable.Id_dl_suggestion_item);
        Component addText = rootView.findComponentById(ResourceTable.Id_right_icon);
        if (dlSuggestionItemComp instanceof DirectionalLayout) {
            DirectionalLayout dlSuggestionItem = (DirectionalLayout) dlSuggestionItemComp;

            dlSuggestionItem.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    onItemClicklistener.onItemClick(itemList.get(position), false);
                }
            });

            addText.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    onItemClicklistener.onItemClick(itemList.get(position), true);
                }
            });
        }
        Component bodyComp = rootView.findComponentById(ResourceTable.Id_body);
        if (bodyComp instanceof Text) {
            Text body = (Text) bodyComp;
            String itemPosition = itemList.get(position);
            body.setText(itemPosition);
        }
        return rootView;
    }
}