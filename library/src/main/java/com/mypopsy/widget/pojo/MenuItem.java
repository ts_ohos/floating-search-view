/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mypopsy.widget.pojo;

public class MenuItem {

    private int id;
    private String name;
    private boolean isNeedCheckbox;
    private boolean isCheckboxChecked;
    private int mFlags = ENABLED;
    private static final int CHECKED = 0x00000002;
    private static final int ENABLED = 0x00000010;

    public MenuItem() {

    }

    public MenuItem(int id, String name, boolean isNeedCheckbox, boolean isCheckboxChecked) {
        this.id = id;
        this.name = name;
        this.isNeedCheckbox = isNeedCheckbox;
        this.isCheckboxChecked = isCheckboxChecked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isNeedCheckbox() {
        return isNeedCheckbox;
    }

    public void setNeedCheckbox(boolean needCheckbox) {
        isNeedCheckbox = needCheckbox;
    }

    public boolean isCheckboxChecked() {
        return isCheckboxChecked;
    }

    public void setCheckboxChecked(boolean checkboxChecked) {
        isCheckboxChecked = checkboxChecked;
    }

    public boolean isChecked() {
        return (mFlags & CHECKED) != 0;
    }

    public MenuItem setChecked(boolean checked) {
        mFlags = (mFlags & ~CHECKED) | (checked ? CHECKED : 0);
        return this;
    }
}
