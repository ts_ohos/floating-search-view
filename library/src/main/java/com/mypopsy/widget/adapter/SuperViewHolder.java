/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mypopsy.widget.adapter;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.global.resource.NotExistException;
import ohos.media.image.PixelMap;
import ohos.utils.PlainArray;
import ohos.utils.net.Uri;
import java.io.IOException;

public class SuperViewHolder implements ChainSetter<SuperViewHolder>{

    private PlainArray<Component> childViews = new PlainArray<>();

    public final Component itemView;

    SuperViewHolder(Component itemView) {
        if (itemView == null) {
            throw new IllegalArgumentException("itemView may not be null");
        }
        this.itemView = itemView;
    }

    public static SuperViewHolder get(Component convertView, Component itemView) {

        SuperViewHolder holder;
        if (convertView == null) {
            holder = new SuperViewHolder(itemView);
            convertView = itemView;
            convertView.setTag(holder);
        } else {
            holder = (SuperViewHolder) convertView.getTag();
        }
        return holder;
    }

    /**
     * Deprecated. Use {@link #findViewById(int)} instead for a better understanding.
     * It will be removed in a future release!
     */
    @Deprecated
    public <T extends Component> T getView(int id) {
        return findViewById(id);
    }

    /**
     * @param id  View id
     * @param <T> Subclass of View
     * @return Child view
     */
    @SuppressWarnings("unchecked")
    public <T extends Component> T findViewById(int id) {
        Component childView = null;
        try {
            if (childViews.get(id).isPresent()) {
                childView = childViews.get(id).get();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (childView == null) {
            childView = itemView.findComponentById(id);
            if (childView != null)
                childViews.put(id, childView);
            else
                return null;
        }
        return (T) childView;
    }

    @Override
    public SuperViewHolder setText(int viewId, String text) {
        Text textView = findViewById(viewId);
        textView.setText(text);
        return this;
    }

    @Override
    public SuperViewHolder setTextColor(int viewId, int textColor) {
        Text textView = findViewById(viewId);
        textView.setTextColor(new Color(textColor));
        return this;
    }

    @Override
    public SuperViewHolder setImageResource(int viewId, int resId) {
        Image view = findViewById(viewId);
        view.setPixelMap(resId);
        return this;
    }

    @Override
    public SuperViewHolder setImageElement(int viewId, Element drawable) {
        Image view = findViewById(viewId);
        view.setImageElement(drawable);
        return this;
    }

    @Override
    public SuperViewHolder setImagePixelMap(int viewId, PixelMap pixelMap) {
        Image view = findViewById(viewId);
        view.setPixelMap(pixelMap);
        return this;
    }

    @Override
    public SuperViewHolder setImageUri(int viewId, Uri imageUri) {
//        Image view = findViewById(viewId);
//        Glide.with(view.getContext())
//                .load(imageUri);
        return this;
    }

    @Override
    public SuperViewHolder setScaleType(int viewId, Image.ScaleMode scaleMode) {
        Image view = findViewById(viewId);
        view.setScaleMode(scaleMode);
        return this;
    }

    @Override
    public SuperViewHolder setBackgroundColor(int viewId, int bgColor) {
        Component view = findViewById(viewId);
        ShapeElement background = new ShapeElement();
        background.setRgbColor(new RgbColor(bgColor));
        view.setBackground(background);
        return this;
    }

    @Override
    public SuperViewHolder setBackgroundResource(int viewId, int bgRes) {
        Component view = findViewById(viewId);
        try {
            view.setBackground(new PixelMapElement(itemView.getContext()
                    .getResourceManager()
                    .getResource(bgRes)));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        }
        return this;
    }

    @Override
    public SuperViewHolder setColorFilter(int viewId, int colorFilter) {
        Image view = findViewById(viewId);
        int[] colors = new int[]{colorFilter, colorFilter};
        int[][] states = new int[2][];
        states[0] = new int[]{0};
        states[1] = new int[]{0};
        ShapeElement element = new ShapeElement();
        element.setStateColorList(states, colors);
        view.setImageElement(element);
        return this;
    }

    @Override
    public SuperViewHolder setAlpha(int viewId, float value) {
        Component view = findViewById(viewId);
        view.setAlpha(value);
        return this;
    }

    @Override
    public SuperViewHolder setVisibility(int viewId, int visibility) {
        Component view = findViewById(viewId);
        view.setVisibility(visibility);
        return this;
    }

    @Override
    public SuperViewHolder setMax(int viewId, int max) {
        ProgressBar view = findViewById(viewId);
        view.setMaxValue(max);
        return this;
    }

    @Override
    public SuperViewHolder setProgress(int viewId, int progress) {
        ProgressBar view = findViewById(viewId);
        view.setProgressValue(progress);
        return this;
    }

    @Override
    public SuperViewHolder setRating(int viewId, int rating) {
        Rating view = findViewById(viewId);
        view.setRatingItems(rating);
        return this;
    }

    @Override
    public SuperViewHolder setTag(int viewId, Object tag) {
        Component view = findViewById(viewId);
        view.setTag(tag);
        return this;
    }

    @Override
    public SuperViewHolder setEnabled(int viewId, boolean enabled) {
        Component view = findViewById(viewId);
        view.setEnabled(enabled);
        return this;
    }

    @Override
    public SuperViewHolder setChecked(int viewId, boolean checked) {
        Component view = findViewById(viewId);
        view.setClickable(checked);
        return this;
    }

    @Override
    public SuperViewHolder setOnClickListener(int viewId, Component.ClickedListener listener) {
        findViewById(viewId).setClickedListener(listener);
        return this;
    }

    @Override
    public SuperViewHolder setOnLongClickListener(int viewId,
                                                  Component.LongClickedListener listener) {
        findViewById(viewId).setLongClickedListener(listener);
        return this;
    }

    @Override
    public SuperViewHolder setOnTouchListener(int viewId, Component.TouchEventListener listener) {
        findViewById(viewId).setTouchEventListener(listener);
        return this;
    }
}
