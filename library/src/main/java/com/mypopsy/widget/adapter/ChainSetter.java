/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mypopsy.widget.adapter;

import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.media.image.PixelMap;
import ohos.utils.net.Uri;

public interface ChainSetter<VH> {

    VH setText(int viewId, String text);

    VH setTextColor(int viewId, int textColor);

//    VH setTextColor(int viewId, ColorStateList colorStateList);

//    VH setMovementMethod(int viewId, MovementMethod method);

    VH setImageResource(int viewId, int resId);

    VH setImageElement(int viewId, Element drawable);

    VH setImagePixelMap(int viewId, PixelMap pixelMap);

    VH setImageUri(int viewId, Uri imageUri);

    VH setScaleType(int viewId, Image.ScaleMode scaleMode);

    VH setBackgroundColor(int viewId, int bgColor);

    VH setBackgroundResource(int viewId, int bgRes);

//    VH setColorFilter(int viewId, ColorFilter colorFilter);

    VH setColorFilter(int viewId, int colorFilter);

    VH setAlpha(int viewId, float value);

    VH setVisibility(int viewId, int visibility);

    VH setMax(int viewId, int max);

    VH setProgress(int viewId, int progress);

    VH setRating(int viewId, int rating);

    VH setTag(int viewId, Object tag);

    VH setEnabled(int viewId, boolean enabled);

//    VH setAdapter(int viewId, BaseItemProvider adapter);

    VH setChecked(int viewId, boolean checked);

    VH setOnClickListener(int viewId, Component.ClickedListener listener);

    VH setOnLongClickListener(int viewId, Component.LongClickedListener listener);

    VH setOnTouchListener(int viewId, Component.TouchEventListener listener);

}
