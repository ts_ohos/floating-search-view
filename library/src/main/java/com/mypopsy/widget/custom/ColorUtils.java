/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mypopsy.widget.custom;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import java.io.IOException;

public class ColorUtils {

    public static RgbColor colorToRgbColor(final int color) {
        int alpha = color >>> 24;
        int r = (color & 0xff0000)>> 16;
        int g = (color & 0xff00)>> 8;
        int b = color & 0xff;
        return new RgbColor(r, g, b, alpha);
    }

    /**
     * 颜色引用转ShapeElement
     * @param context
     * @param resId 引用资源
     * @return
     */
    public static ShapeElement colorResToElement(Context context, int resId) {
        ShapeElement element = new ShapeElement();
        try {
            int color = context.getResourceManager()
                    .getElement(resId)
                    .getColor();
            element.setRgbColor(ColorUtils.colorToRgbColor(color));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        return element;
    }

    /**
     * 颜色资源转Color
     * @param context
     * @param resId
     * @return
     */
    public static Color colorResToColor(Context context, int resId){
        try {
            int color = context.getResourceManager()
                    .getElement(resId)
                    .getColor();
            return new Color(color);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        return Color.BLACK;
    }

}
