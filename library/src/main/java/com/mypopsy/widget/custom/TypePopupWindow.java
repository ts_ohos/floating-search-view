/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mypopsy.widget.custom;

import com.mypopsy.widget.ResourceTable;
import com.mypopsy.widget.adapter.BaseListAdapter;
import com.mypopsy.widget.adapter.OnListItemClickListener;
import com.mypopsy.widget.adapter.SuperViewHolder;
import com.mypopsy.widget.pojo.MenuItem;
import com.mypopsy.widget.utils.PxUtils;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.PopupDialog;
import ohos.app.Context;
import java.util.List;

public class TypePopupWindow extends PopupDialog {

    private Context mContext;
    private Component parent;
    private ListContainer recyclerView;
    private List<MenuItem> data;
    private MenuItem classification; //当前显示的分类
    private OnProjectTypeClickListener listener;

    public TypePopupWindow(Context context) {
        super(context, null);
        this.mContext = context;
        setBackColor(Color.TRANSPARENT);
        setHasArrow(true);
    }

    public TypePopupWindow(Context context, Component contentComponent) {
        this(context, contentComponent,
                PxUtils.getScreenWidth(context),
                PxUtils.getScreenHeight(context));
    }

    public TypePopupWindow(Context context, Component contentComponent,
                           int width, int height) {
        super(context, contentComponent, width, height);
        this.mContext = context;
        setBackColor(Color.TRANSPARENT);
        setHasArrow(true);
    }

    public TypePopupWindow setData(List<MenuItem> data) {
        this.data = data;
        return this;
    }

    public TypePopupWindow setCurrent(MenuItem classification) {
        this.classification = classification;
        return this;
    }


    public TypePopupWindow setListener(OnProjectTypeClickListener listener) {
        this.listener = listener;
        return this;
    }

    public void show(Component view) {
        this.parent = view;
        initView();
    }

    private void initView() {

        Component childView = LayoutScatter.getInstance(mContext)
                .parse(ResourceTable.Layout_wan_pop_project_pop,
                        null,
                        false);
        childView.findComponentById(ResourceTable.Id_view)
                .setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                hide();
            }
        });
        recyclerView = (ListContainer) childView.findComponentById(ResourceTable.Id_recycle_view);
        setCustomComponent(childView);
        setMode(LayoutAlignment.RIGHT);
        show();
        initList();
    }

    private void initList() {
        recyclerView.setItemProvider(new TypeAdapter(mContext, data, listener,
                new OnListItemClickListener<MenuItem>() {
            @Override
            public void onItemClick(MenuItem data, int position) {
                hide();
                if (listener != null) {
                    listener.onProjectTypeClick(data);
                }
            }
        }));
    }

    class TypeAdapter extends BaseListAdapter<MenuItem> {

        private Context context;
        private OnProjectTypeClickListener listener;

        /**
         * 常规列表重写该方法
         *
         * @param context     context
         * @param list        数据源
         * @param listener    Item点击回调
         */
        public TypeAdapter(Context context, List<MenuItem> list,
                           OnProjectTypeClickListener onProjectTypeClickListener,
                           OnListItemClickListener<MenuItem> listener) {
            super(context, list, ResourceTable.Layout_item_of_setup_list, listener);
            this.context = context;
            this.listener = onProjectTypeClickListener;
        }

        @Override
        public void onBindData(
                SuperViewHolder holder, int layoutPosition,
                MenuItem data) {
            Text textView = holder.findViewById(ResourceTable.Id_tv_text);
            Checkbox checkbox = holder.findViewById(ResourceTable.Id_checkbox);
            if (data.isNeedCheckbox()) {
                checkbox.setVisibility(Component.VISIBLE);
                checkbox.setClickedListener(v -> {
                    hide();
                    if (listener != null) {
                        listener.onProjectTypeClick(data);
                    }

                });
            }
            if (data.isCheckboxChecked()) {
                checkbox.setChecked(true);
            }
            if (classification != null && classification.getId() == data.getId()) {
                textView.setTextColor(ColorUtils.colorResToColor(context,
                        ResourceTable.Color_white));
                textView.setBackground(ColorUtils.colorResToElement(context,
                        ResourceTable.Color_theme));
            } else {
                textView.setTextColor(ColorUtils.colorResToColor(context,
                        ResourceTable.Color_black));
                textView.setBackground(ColorUtils.colorResToElement(context,
                        ResourceTable.Color_transparent));
            }
            textView.setText(data.getName());
        }
    }

    public interface OnProjectTypeClickListener {
        void onProjectTypeClick(MenuItem classification);
    }
}
