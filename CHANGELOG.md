# Changelog
本组件基于原库master版本移植(5a24e73 on 10 Jun 2016 Git stats)

## v.1.0.0
移植后首次发布上传
## v.1.0.1
新增文件增加版权头

## support
原库UI样式基本全部移植

## not support
1、不支持语音输入搜索功能，鸿蒙语音识别文档暂不完善，实现起来有些问题。
2、加载动画实现是通过android中自带的ActionMenuView实现的，鸿蒙暂不支持。